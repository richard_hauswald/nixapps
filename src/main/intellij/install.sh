#!/bin/sh
set -e
set -u

install_kiss_program() {
    local install_directory="$1"
    local install_base_directory="$2"
    local download_url="$3"
    local installation_link="${install_base_directory}/current"
    local download_temp_directory="/tmp/kiss_programm_archive_$(basename "${install_base_directory}")_"
    local archive_file="${download_temp_directory}/app.tar.gz"

    rm -rf "${download_temp_directory}"
    mkdir "${download_temp_directory}"
    local axel_binary="$(which axel)"
    if [ "${axel_binary}" != "" ]; then
	axel -a -n10 -o "${archive_file}" "${download_url}"
    else
	wget "${download_url}" -O "${archive_file}"
    fi

    rm -f "${installation_link}"
    rm -rf "${install_directory}"

    mkdir -p "${install_base_directory}/"
    tar -xzf "$archive_file" -C "${download_temp_directory}"
    rm "${archive_file}"

    local number_of_files_in_archive=$(ls -l "${download_temp_directory}" | wc -l)
    if [ "${number_of_files_in_archive}" != "2" ]; then
	echo "This installer only supports tar archives containing exactly one root entry" >&2
	exit 1
    fi

    app_version_directory="$(ls "${download_temp_directory}")"
    mv "${download_temp_directory}/${app_version_directory}" "${install_directory}"
    rm -rf "${download_temp_directory}"

    ln -s "${install_directory}" "${installation_link}"
}

determine_app_base_directory() {
    local ret="$( cd ~/apps/intellij && pwd )"
    echo "$ret"
}
app_base_directory="`determine_app_base_directory`"


. "${app_base_directory}/default_version"
[ -e "${app_base_directory}/custom_version" ] && . "${app_base_directory}/custom_version"

install_directory="${app_base_directory}/installed_versions/idea-IU-${VERSION}"
installation_link="${app_base_directory}/installed_versions/current"
website_url="https://confluence.jetbrains.com/display/IDEADEV/IDEA+14.1+EAP"
download_url="$(wget -O- "${website_url}" 2>/dev/null | grep "ideaIU-${VERSION}.tar.gz\"" | awk -v FS="href=" '{ print $2 }' | cut -d '"' -f2)"

if [ "${download_url}" = "" ]; then
    new_version="$(wget -O- "${website_url}" 2>/dev/null | grep -e "ideaIU-[^\"]*tar.gz\"" | head -n1 | awk -v FS="\">" '{ print $2 }' | awk -v FS="ideaIU-" '{ print $2 }' | awk -v FS=".tar.gz" '{ print $1 }')"
    if [ "${new_version}" != "" ]; then
        echo "Could not find requested version '${VERSION}' download link. However the version '${new_version}' is listed at " \
        "jetbrains eap wiki page. If you want to install that version ensure the file ${app_base_directory}/custom_version" \
        "contains only the line 'VERSION=${new_version}'" \ >&2
    else
        echo "Could not find requested version download link. " >&2
    fi
    exit 1
fi

install_kiss_program "$install_directory" "$app_base_directory/installed_versions" "$download_url"
