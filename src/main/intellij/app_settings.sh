determine_app_base_directory() {
    local ret="$( cd ~/apps/intellij && pwd )"
    echo "$ret"
}

determine_home_directory() {
    local ret="$( cd ~/ && pwd )"
    echo "$ret"
}

app_base_directory="`determine_app_base_directory`"
home_directory="`determine_home_directory`"

export INTELLIJ_RUN_PATH="$app_base_directory/installed_versions/current/bin/idea.sh"
export INTELLIJ_CONFIG_PATH="$home_directory/.IntelliJIdea14/config"
export PATH="$PATH:${app_base_directory}/launcher"

idea_launcher_path="$(which idea)"
if [ "$?" != 0 ]; then
	echo "idea launcher binary not found in path" >&2
elif [ "${idea_launcher_path}" != "$home_directory/apps/intellij/launcher/idea" ]; then
	echo "idea launcher binary found, but it's location '${idea_launcher_path}' differs from expected location '$home_directory/apps/intellij/launcher/idea'" >&2
fi
