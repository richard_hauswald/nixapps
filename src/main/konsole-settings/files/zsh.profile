[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=staenker
Font=Monospace,10,-1,5,50,0,0,0,0,0

[General]
Command=/bin/zsh
Name=zsh
Parent=FALLBACK/

[Scrolling]
HistorySize=10000
ScrollBarPosition=1
