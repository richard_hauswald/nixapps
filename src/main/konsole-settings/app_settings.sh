determine_app_base_directory() {
    local ret="$( cd ~/apps/konsole-settings && pwd )"
    echo "$ret"
}

determine_home_directory() {
    local ret="$( cd ~/ && pwd )"
    echo "$ret"
}
app_base_directory="`determine_app_base_directory`"

files_directory="${app_base_directory}/files"

home_directory="`determine_home_directory`"
konsole_files_directory="$home_directory/.kde/share/apps/konsole"
if [ -e "$konsole_files_directory" ]; then
	if [ "$(ls -A "$konsole_files_directory")" != "" ]; then
		for existing_file in "$konsole_files_directory"/*; do
			existing_file_basename="$(basename "$existing_file")"
			source_file="$files_directory/$existing_file_basename"
			if [ -e "$source_file" ]; then
				source_file_md5_sum="$(md5sum "$source_file" | awk '{print $1}')"
				existing_file_md5_sum="$(md5sum "$existing_file" | awk '{print $1}')"
				if [ "$source_file_md5_sum" != "$existing_file_md5_sum" ]; then
					rm "$existing_file"
				fi
			else
				rm "$existing_file"
			fi
		done
	fi
else
	mkdir -p "$konsole_files_directory"
fi

for file in "$files_directory"/*; do
	TARGET_FILE="$konsole_files_directory/$(basename $file)"
	cp "$file" "$TARGET_FILE"
done

[ -e "$home_directory"/.kde/share/config/konsolerc ] || cp "$app_base_directory"/konsolerc "$home_directory"/.kde/share/config/konsolerc
source_file_md5_sum="$(md5sum "$app_base_directory"/konsolerc | awk '{print $1}')"
existing_file_md5_sum="$(md5sum "$home_directory"/.kde/share/config/konsolerc | awk '{print $1}')"
if [ "$source_file_md5_sum" != "$existing_file_md5_sum" ]; then
	cp "$app_base_directory"/konsolerc "$home_directory"/.kde/share/config/konsolerc
fi

