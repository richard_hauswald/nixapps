determine_app_base_directory() {
    local ret="$( cd ~/apps/maven && pwd )"
    echo "$ret"
}
app_base_directory="`determine_app_base_directory`"

. "${app_base_directory}/default_version"
[ -e "${app_base_directory}/custom_version" ] && . "${app_base_directory}/custom_version"

if [ -e "${app_base_directory}/installed_versions/current" ]; then
    export M2_HOME="$app_base_directory/installed_versions/current"
    export MAVEN_OPTS="-Xms256m -Xmx256m"
    export PATH="$PATH:$M2_HOME/bin"

    mvn_version="$(mvn -version | grep "Apache Maven" | awk '{print $3}')"
    if [ "$?" = "0" ]; then
        if [ "$mvn_version" != "$VERSION" ]; then
             echo "current maven installation is running version '$mvn_version' but the expected version is '$VERSION'" \
              "This usually means there is an update available. It might also be that you naughty dev have decided to" \
              "to install your own preferred version and didn't mention it in the file '${app_base_directory}/custom_version'" \
              "which should contain the line 'VERSION=${mvn_version}'" >&2
         fi
    else
        echo "app check command 'mvn -version' failed. This means that this app is broken and you poor dev have to do some debugging now!" >&2
    fi
else
    echo "no maven version installed! run '$app_base_directory/install.sh' to install it" >&2
fi
