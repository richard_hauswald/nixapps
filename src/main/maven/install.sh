#!/bin/sh
set -e
set -u

install_kiss_program() {
    local install_directory="$1"
    local install_base_directory="$2"
    local download_url="$3"
    local installation_link="${install_base_directory}/current"
    local archive_file="/tmp/kiss_programm_archive.tar.gz"

    wget "${download_url}" -O "${archive_file}"
    rm -f "${installation_link}"
    rm -rf "${install_directory}"

    tar -xzf "$archive_file" -C "${install_base_directory}"
    rm "${archive_file}"

    ln -s "${install_directory}" "${installation_link}"
}

determine_app_base_directory() {
    local ret="$( cd ~/apps/maven && pwd )"
    echo "$ret"
}
app_base_directory="`determine_app_base_directory`"

. "${app_base_directory}/default_version"
[ -e "${app_base_directory}/custom_version" ] && . "${app_base_directory}/custom_version"

mkdir -p "$app_base_directory/installed_versions"
install_directory="${app_base_directory}/installed_versions/apache-maven-${VERSION}"
installation_link="${app_base_directory}/installed_versions/current"
download_url="`wget http://maven.apache.org/download.cgi -O- 2>/dev/null | grep "${VERSION}" | grep "bin.tar.gz</a>" | grep "binaries" | cut -d '<' -f 3 | cut -d '"' -f 2`"

if [ "${download_url}" = "" ]; then
    echo "Could not find requested version download link" >&2
    exit 1
fi

install_kiss_program "$install_directory" "$app_base_directory/installed_versions" "$download_url"
