#!/bin/sh

OLD_EMAIL="$1"
NEW_EMAIL="$2"

set -u
set -e

usage() {
    echo "usage: changeAutherEmail <old-email> <new-email>" &>2
    exit 1
}

if test -z ${OLD_EMAIL};then
    usage
elif test -z ${NEW_EMAIL}; then
    usage
fi

git filter-branch -f --env-filter '
an="$GIT_AUTHOR_NAME"
am="$GIT_AUTHOR_EMAIL"
cn="$GIT_COMMITTER_NAME"
cm="$GIT_COMMITTER_EMAIL"

if [ "$GIT_COMMITTER_EMAIL" = "'${OLD_EMAIL}'" ]
then
cm="'${NEW_EMAIL}'"
echo "changed author from $GIT_COMMITTER_EMAIL to $cm"
fi
if [ "$GIT_AUTHOR_EMAIL" = "'${OLD_EMAIL}'" ]
then
am="'${NEW_EMAIL}'"
echo "changed author from $GIT_AUTHOR_EMAIL to $am"
fi

export GIT_AUTHOR_NAME="$an"
export GIT_AUTHOR_EMAIL="$am"
export GIT_COMMITTER_NAME="$cn"
export GIT_COMMITTER_EMAIL="$cm"
'
