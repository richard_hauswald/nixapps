git_binary="$(which git)"
if test -z "$git_binary"; then
	echo "git binary not found"
	return
fi
alias git_list_last_100='git log --pretty=format:"%h%x09%an%x09%ad%x09%s" | head -n 100'
git_username="$(git config -l | grep user.name | cut -d '=' -f2)"
if test -n "$git_username"; then
	alias git_list_my_last_100="git log --pretty=format:\"%h%x09%an%x09%ad%x09%s\" | grep \"$git_username\" | head -n 100"
else
	echo "git username not configured - alias git_list_my_last_100 will not be available"
	return
fi
