#!/bin/sh
STARTED_FROM_DIRECTORY="`pwd`"

for file in `find ~/apps/* -name app_settings.sh`; do
	echo "Loading settings $file"
	cd "`dirname $file`"
	. "$file"
done

cd "$STARTED_FROM_DIRECTORY"
